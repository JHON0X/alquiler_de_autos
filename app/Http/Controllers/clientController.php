<?php

namespace App\Http\Controllers;
use App\Model\client;

use Illuminate\Http\Request;
use DB;
use Input;
use Storage;

class clientController extends Controller
{ 
    //
    public function index(){
        return client::all();
    }
    public function create(Request $request){
        $clients = new client();

        $clients->names= $request->input('names');
        $clients->lastname= $request->input('lastname');
        $clients->ci= $request->input('ci');
        $clients->address= $request->input('address');
        $clients->save();

        return response()->json($clients);


    }
    public function store(Request $request){

        $this->validate($request,[ 'names'=>'required', 'lastname'=>'required', 'ci'=>'required', 'address'=>'required']);
        return client::create($request->all());
        /*
        $client = new Client();
        $client->fill($request->toArray())->save();
        return $client;
    */
    }
    public function update(Request $request, $id)    {
        //
        $this->validate($request,[ 'names'=>'required', 'lastname'=>'required', 'ci'=>'required', 'address'=>'required']);
 
        return client::find($id)->update($request->all());
    
 
    }
    public function destroy($id){

        return client::find($id)->delete();
        
        /*
        $client = Client::query()->where('id_client',$id)->first();
        return $client->delete();
*/
    }
    
}
