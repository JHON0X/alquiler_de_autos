<?php

namespace App\Http\Controllers;
use App\Model\detail;
use Illuminate\Http\Request;

class detailController extends Controller
{
    //
    public function show(){
        return detail::all();

    }
    public function index(){
        return detail::all();

    }
    public function create(request $request){
        $detail= new detail();
        $detail->date_rent = $request->date_rent;
        $detail->time_rent = $request->time_rent;
        $detail->can_detail_rent = $request->can_detail_rent;
        $detail->save();
        return "guardado";
    
    }
    public function update(request $request,$id_detail){
        $detail= $request->date_rent;
        $detail= $request->time_rent;
        $detail= $request->can_detail_rent;
        $detail= detail::find($id_detail);
        $detail= $request->date_rent;
        $detail= $request->time_rent;
        $detail= $request->can_detail_rent;
        $detail->save();
        return "editado";
    
    }
    public function delete($id_detail){

        $detail= detail::find($id_detail);
        $detail->delete();
        
        return "eliminado";
    }
}
