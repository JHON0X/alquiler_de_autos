<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class makecar extends Model
{
    protected $table= 'makecar';
    protected $primarykey= 'id_make';
    public $timestamps= true;
    const CREATED_AT = 'date_create';
    const UPDATED_AT = 'date_upadate';

    protected $fillable= [
        'date_make',
        'name_car',
        'model_car',

    ];
}
