<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('client', 'clientController');
Route::resource('car', 'carController');
Route::resource('cost', 'costController');
Route::resource('detail', 'detailController');
Route::resource('makecar', 'makecarController');
Route::resource('order', 'orderController');
Route::resource('usser', 'usserController');

