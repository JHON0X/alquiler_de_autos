<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');

});

Route::get('/client','clientController@show');
Route::get('/car','carController@show');
Route::get('/cost','costController@show');
Route::get('/detail','detailController@show');
Route::get('/makecar','makecarController@show');
Route::get('/order','orderController@show');

//Route::resource();
